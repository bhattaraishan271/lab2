import unittest
from mergesort import mergeSort
from mergesort import merge

class SortTestCase(unittest.TestCase):
	def testMergeSort(self):
		data = [2, 1, 6, 4, 0]
		sorteddata =  [0, 1, 2, 4, 6]
		mergeSort(data)
		self.assertListEqual(data, sorteddata)
	def testMerge(self):
		L = [2,4]
		R = [0,9]
		data = [2, 4, 0, 9]
		mergeddata =  [0, 2, 4, 9]
		merge(L, R, data)
		self.assertListEqual(data, mergeddata)

if __name__ == '__main__':
	unittest.main()