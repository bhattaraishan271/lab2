from mergesort import mergeSort
import random
from time import time
import matplotlib.pyplot as plt

n = 10000
i = 0
timeArray = []
sizeArray = []

for i in range(n, n * 11, n):
	sizeArray.append(i)
	randomvalues = random.sample(range(i),i)
	starttime = time()
	# randomvalues.sort() #best case
	mergeSort(randomvalues)
	endtime = time()
	totaltime = endtime - starttime
	timeArray.append(totaltime)
	print(totaltime, "for size", i)

def getTimeArray():
	return timeArray

if __name__ == '__main__':
	fig, ax = plt.subplots(1,1)
	ax.plot(sizeArray, timeArray)
	plt.show()