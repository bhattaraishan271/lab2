#merging function
def merge(L,R, alist):
	i = j = k = 0
	while i < len(L) and j < len(R):
			if L[i] < R[j]:
				alist[k] = L[i]
				i += 1
			else:
				alist[k] = R[j]
				j += 1
			k+=1

	while i < len(L):
		alist[k] = L[i]
		i += 1
		k += 1

	while j < len(R):
		alist[k] = R[j]
		j += 1
		k += 1

# main sorting function
def mergeSort(alist):
	if len(alist) > 1:
		mid = len(alist)//2
		L = alist[:mid]
		R = alist[mid:]

		mergeSort(L)
		mergeSort(R)

		merge(L, R, alist)

if __name__ == '__main__': 
    arr = [12, 11, 13, 5, 6, 7]  
    print ("Given array is", end="\n")  
    print(arr) 
    mergeSort(arr) 
    print("Sorted array is: ", end="\n") 
    print(arr) 
